# petrified_log

TODO: Write a description here

## Installation

1. Add the dependency to your `shard.yml`:

```yaml
dependencies:
  petrified_log:
    git: https://bitbucket.org/barragoon/petrifiedlog
```

2. Run `shards install`

## Config
```crystal
logfile = File.new(path_to_log, "a")
@start = Logger.new(logfile)
@start.level = Logger::DEBUG
@start.level = Logger::INFO
@start.level = Logger::WARN
@start.level = Logger::ERROR
@start.level = Logger::FATAL
@start.level = Logger::UNKNOWN
@start.unknown "--------- start ----------"

@start.debug "1 debug"
@start.info "2 info"
@start.warn "3 warn"
@start.error "4 error"
@start.fatal "5 fatal"
@start.unknown "6 unknown"
```

## Usage

```crystal
require "petrified_log"

class L
  def self.l
    k = PetrifiedLog::Writer.instance.start
    k.warn "Lorem ipsum"
  end
end

class O
  def initialize
    k = PetrifiedLog::Writer.instance.start
    k.warn "O-ojej"
  end
end

PetrifiedLog::Writer.log_path "./custom.log"
PetrifiedLog::Writer.instance.start
O.new
L.l
```

## Development

TODO: Write development instructions here

## Contributing

1. Fork it (<https://github.com/your-github-user/petrified_log/fork>)
2. Create your feature branch (`git checkout -b my-new-feature`)
3. Commit your changes (`git commit -am 'Add some feature'`)
4. Push to the branch (`git push origin my-new-feature`)
5. Create a new Pull Request

## Contributors

- [jez](https://github.com/your-github-user) - creator and maintainer
