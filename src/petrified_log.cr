require "logger"

# TODO: Write documentation for `PetrifiedLog`
module PetrifiedLog
  VERSION = "0.1.0"

    class Writer
    property start : Logger

    private def initialize(path_to_log)
      logfile = File.new(path_to_log, "a")
      @start = Logger.new(logfile)
      @start.level = Logger::DEBUG
      # @start.level = Logger::INFO
      # @start.level = Logger::WARN
      # @start.level = Logger::ERROR
      # @start.level = Logger::FATAL
      # @start.level = Logger::UNKNOWN
      @start.unknown "--------- start ----------"

      # @start.debug "1 debug"
      # @start.info "2 info"
      # @start.warn "3 warn"
      # @start.error "4 error"
      # @start.fatal "5 fatal"
      # @start.unknown "6 unknown"
    end

    def self.instance
      @@instance ||= new log_path
    end

    def self.log_path
      @@log_path ||= "./default.log"
    end

    def self.log_path(path : String)
      @@log_path = path
    end
  end
end
